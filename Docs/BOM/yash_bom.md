# yash.kicad_sch BOM

Tue 24 Oct 2023 08:04:53 PM EDT

Generated from schematic by Eeschema 7.0.8-7.0.8~ubuntu22.04.1

**Component Count:** 49

| Refs | Qty | Component | Description | Vendor | SKU | Note |
| ----- | --- | ---- | ----------- | ---- | ---- | ---- |
| C1 | 1 | 10nF (PP) | Polypropylene capacitor | DigiKey | FKP2G011001D00HSSD |  |
| C2 | 1 | 4.7nF | Ceramic capacitor, 2.5 mm pitch | Tayda |  |  |
| C3 | 1 | 470nF | Polyester capacitor | Tayda | A-568 |  |
| C4, C6, C7 | 3 | 100nF | Ceramic capacitor, 2.5 mm pitch | Tayda | A-553 |  |
| C5, C8 | 2 | 10uF | Electrolytic capacitor, 2.5 mm pitch | Tayda | A-4349 |  |
| D1 | 1 | LED_green | Light emitting diode | Tayda | A-1553 |  |
| D2, D3 | 2 | 1N4148 | Standard switching diode, DO-35 | Tayda | A-157 |  |
| D5, D6 | 2 | 1N5817 | Schottky Barrier Rectifier Diode, DO-41 | Tayda | A-159 |  |
| GRAF1 | 1 | Holes | Graphic |  |  |  |
| J1, J2, J4 | 3 | AudioJack2 | 1/4" audio jack, vertical | Tayda | A-1121 |  |
| J3 | 1 | AudioJack2_SwitchT | 1/4" audio jack, vertical, switched | Tayda | A-1121 |  |
| J5 | 1 | 3_pin_Molex_header | KK254 Molex header | Tayda | A-805 |  |
| J6 | 1 | 3_pin_Molex_connector | KK254 Molex connector | Tayda | A-827 |  |
| J7, J10 | 2 | 2_pin_Molex_header | KK254 Molex header | Tayda | A-804 |  |
| J8, J11 | 2 | 2_pin_Molex_connector | KK254 Molex connector | Tayda | A-826 |  |
| J9 | 1 | Synth_power_2x5 | Pin header 2.54 mm 2x5 | Tayda | A-2939 |  |
| J12 | 1 | DIP-8 | 8 pin DIP socket |  |  |  |
| J13 | 1 | DIP-14 | 14 pin DIP socket | Tayda | A-004 |  |
| Q1 | 1 | 2N3904 | Small Signal NPN Transistor, TO-92 | Tayda | A-111 |  |
| R1 | 1 | 1k | 1/4 watt resistor, metal film | Tayda |  |  |
| R2, R8 | 2 | 100k | 1/4 watt resistor, metal film | Tayda |  |  |
| R3 | 1 | RL | 1/4 watt resistor, metal film | Tayda |  | Use value for suitable LED brightness, I used 2k with the BOM LED |
| R4 | 1 | 5.6k | 1/4 watt resistor, metal film | Tayda |  |  |
| R5 | 1 | 4.7k | 1/4 watt resistor, metal film | Tayda |  |  |
| R6 | 1 | 10k | 1/4 watt resistor, metal film | Tayda |  |  |
| R7 | 1 | 1.8k | 1/4 watt resistor, metal film | Tayda |  |  |
| R9 | 1 | 2.7k | 1/4 watt resistor, metal film | Tayda |  |  |
| R10, R12 | 2 | 2.2k | 1/4 watt resistor, metal film | Tayda |  |  |
| RV1 | 1 | 100k | Linear potentiometer, panel mount | Tayda | A-1984 |  |
| RV2 | 1 | 1M | Linear potentiometer, panel mount | Tayda | A-1658 |  |
| SW1 | 1 | SW_SPDT | SPDT or DPDT toggle switch | Tayda | A-3186 |  |
| TP1 | 1 | -12V | test point |  |  |  |
| TP2 | 1 | +12V | test point |  |  |  |
| TP3 | 1 | GND | test point |  |  |  |
| U1 | 1 | LF398 | Monolithic sample-and-hold | Digi-Key | LF398AN/NOPB-ND |  |
| U2 | 1 | 4093 | Quad 2-Input NAND Schmitt Trigger, DIP-14 | Tayda | A-554 |  |
| ZKN1, ZKN2 | 2 | Knob_MF-A04 | Knob |  |  |  |

## Resistors in color band order
|Value|Qty|Refs|
|----|----|----|
|1k|1|R1|
|10k|1|R6|
|100k|2|R2, R8|
|1.8k|1|R7|
|2.2k|2|R10, R12|
|2.7k|1|R9|
|4.7k|1|R5|
|5.6k|1|R4|
|RL|1|R3|

