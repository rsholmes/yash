# YASH

This is a Kosmo format version of René Schmitz's [YASH](https://www.schmitzbits.de/sah.html) sample and hold synth module.

The circuit is pretty much as Schmitz designed it. The only significant changes are:

* Addition of an output jack for the clock signal
* Addition of an attenuator for the input signal
* Addition of a switch to bypass the capacitor on the clock input. This turns the module from a sample and hold into a track and hold: applying a gate to the clock input causes the output to track the input voltage, and then when the gate turns off the last output voltage is held until the next gate.
* Values of integration and gate timing capacitors were multiplied by 10. This results in 10x slower droop rate; using Schmitz's values the droop in 10 seconds is quite large.

## Current draw
11.3 mA +12 V, 4.0 mA -12 V


## Photos

![](Images/front.jpg)

![](Images/back.jpg)

## Documentation

* [Schematic](Docs/yash_schematic.pdf)
* PCB layout: [front](Docs/Layout/yash/yash_F.SilkS.pdf), [back](Docs/Layout/yash/yash_B.SilkS.pdf)
* [BOM](Docs/BOM/yash_bom.md)
* [Build notes](Docs/build.md)
* [Blog post](https://analogoutputblog.wordpress.com/2022/05/06/yet-another-sample-and-hold/)

## Git repository

* [https://gitlab.com/rsholmes/yash](https://gitlab.com/rsholmes/yash)

